use std::{path::Path, process::Command};

fn main() {
    compile_java();
    compile_c();
    compile_objc();
    compile_swift();
    link_libraries();

    // Run the Java command after the build
    if let Err(e) = run_java_command() {
        panic!("Failed to run Java command: {:?}", e);
    }
}

fn compile_java() {
    // Compile Java code using `javac`
    println!("cargo:rerun-if-changed=java/HelloWorld.java");
    let java_compile_status = Command::new("javac")
        .arg("-d")
        .arg("java") // Output directory for .class files
        .arg("java/HelloWorld.java")
        .status()
        .expect("Failed to compile Java code");
    if !java_compile_status.success() {
        panic!("Java compilation failed");
    }
}
fn run_java_command() -> std::io::Result<()> {
    let java_class_path = "./java";
    let java_library_path = "./target/release";

    Command::new("java")
        .arg("-cp")
        .arg(java_class_path)
        .arg("-Djava.library.path")
        .arg(java_library_path)
        .arg("HelloWorld")
        .status()
        .map(|_| ())
}

fn compile_c() {
    // Compile C code
    cc::Build::new()
        .file("c/example.c")
        .include("c") // Include directory for header files
        .compile("example");
}

fn compile_objc() {
    // Compile Objective-C code
    cc::Build::new()
        .file("objc/MyClass.m") // Path to your Objective-C source file
        .includes(&["objc"]) // Include necessary directories
        .flag("-ObjC") // Objective-C flag for linking all categories and classes
        .compile("MyClass"); // Name of the compiled library (without extension)

    // Link to the Objective-C runtime
    println!("cargo:rustc-link-lib=objc"); // Link against the Objective-C runtime library

    // Specify the path to the Objective-C header files
    println!("cargo:include=objc"); // Include directory for headers
}

fn compile_swift() {
    // Compile Swift code using `swiftc`
    println!("cargo:rerun-if-changed=swift/MySwiftClass.swift");
    let swift_output_dir = "swift";
    let swift_file = "swift/MySwiftClass.swift";
    let swift_lib = "libSwiftModule.dylib";

    // Ensure swift directory exists
    if !Path::new(swift_output_dir).exists() {
        std::fs::create_dir_all(swift_output_dir).expect("Failed to create Swift output directory");
    }

    Command::new("swiftc")
        .args(&[
            "-emit-library",
            "-emit-module",
            "-module-name",
            "SwiftModule",
            "-o",
            swift_lib,
            swift_file,
        ])
        .current_dir(swift_output_dir)
        .status()
        .expect("Failed to compile Swift code");
}

fn link_libraries() {
    // Link to the compiled C, Objective-C, and Swift libraries
    println!("cargo:rustc-link-lib=static=example");
    println!("cargo:rustc-link-lib=objc");
    println!("cargo:rustc-link-lib=dylib=SwiftModule"); // Link the Swift library
    println!("cargo:rustc-link-search=native=swift"); // Search path for Swift library
}
