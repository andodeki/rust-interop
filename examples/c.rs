// examples/c.rs
// sudo cargo run --example c --release

fn main() {
    unsafe {
        rust_interop::add_and_print(2, 4); // Call the function from rust_interop
    }
}
