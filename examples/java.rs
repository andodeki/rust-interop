use std::process::Command;

fn main() {
    // Run the Java command after the build
    if let Err(e) = run_java_command() {
        panic!("Failed to run Java command: {:?}", e);
    }
}
fn run_java_command() -> std::io::Result<()> {
    let java_class_path = "./java";
    let java_library_path = "./target/release";

    let status = Command::new("java")
        .arg("-cp")
        .arg(java_class_path)
        .arg(format!("-Djava.library.path={}", java_library_path))
        .arg("HelloWorld")
        .status()?;

    if !status.success() {
        panic!("Java execution failed");
    }

    Ok(())
}
