use rust_interop::swift::swiftFunction;

fn main() {
    unsafe {
        // let result = rust_interop::swift::swiftFunction();
        let result = swiftFunction();
        println!("Result from Swift function: {}", result);
    }
}
