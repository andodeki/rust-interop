// java/HelloWorld.java
public class HelloWorld {
    static {
        System.loadLibrary("rust_interop");
    }

    public native void callRustFunction();

    public String getGreeting() {
        return "Hello from Java!";
    }

    public static void main(String[] args) {
        HelloWorld hw = new HelloWorld();
        hw.callRustFunction();
    }
}
