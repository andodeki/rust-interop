// java/Main.java
public class Main {
    static {
        System.loadLibrary("rust_jni_example");
    }

    private static native void call_java_from_rust();

    public static void main(String[] args) {
        System.out.println("Calling Rust function...");
        call_java_from_rust();
        System.out.println("Rust function call completed.");
    }
}
