// swift/MyClass.h
#import <Foundation/Foundation.h>

#ifdef __cplusplus
extern "C" {
#endif

// Declare functions to be used by Rust
void create_object();
void call_method();
const char* get_message();

#ifdef __cplusplus
}
#endif

@interface MyClass : NSObject
- (void)printMessage;
- (NSString *)getMessage;
@end
