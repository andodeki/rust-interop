// swift/MyClass.m
#import "MyClass.h"

@implementation MyClass

- (void)printMessage {
    NSLog(@"Hello from Objective-C!");
}

- (NSString *)getMessage {
    return @"Hello from Objective-C!";
}

@end

// C-compatible interface functions
static MyClass *myClassInstance = nil;

void create_object() {
    myClassInstance = [[MyClass alloc] init];
}

void call_method() {
    if (myClassInstance) {
        [myClassInstance printMessage];
    }
}

const char* get_message() {
    if (myClassInstance) {
        NSString *message = [myClassInstance getMessage];
        return [message UTF8String];
    }
    return "";
}
