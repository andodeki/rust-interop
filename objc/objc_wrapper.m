#import "objc_wrapper.h"
#import "MyClass.h"

void call_objc_function() {
    MyClass *obj = [[MyClass alloc] init];
    [obj helloFromObjectiveC];
}
