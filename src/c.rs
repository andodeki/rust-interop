// Declare external functions from C
extern "C" {
    pub fn add(a: i32, b: i32) -> i32;
    pub fn print_message();
}
