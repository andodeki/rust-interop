use makepad_android_state::get_java_vm;
use makepad_jni_sys::{jint, jobject, JavaVM, _jmethodID, _jobject};
use std::{
    ffi::{CStr, CString},
    ptr,
};

use makepad_jni_sys as jni_sys;

static mut _ACTIVITY: jobject = ptr::null_mut();
static mut VM: *mut JavaVM = ptr::null_mut();

#[no_mangle]
pub extern "C" fn call_java_from_rust() {
    unsafe {
        // Get the JavaVM instance
        if get_java_vm().is_null() {
            eprintln!("Failed to get JavaVM");
            return;
        }

        let env = attach_jni_env();

        let class_name = CString::new("HelloWorld").unwrap();
        let method_name = CString::new("getGreeting").unwrap();
        let method_sig = CString::new("()Ljava/lang/String;").unwrap();
        let (obj, method_id) = new_object(env, class_name, method_name, method_sig);

        // let method_name = CString::new("getGreeting").unwrap();
        // let method_sig = CString::new("()Ljava/lang/String;").unwrap();
        // let method_id =
        //     (**env).GetMethodID.unwrap()(env, class, method_name.as_ptr(), method_sig.as_ptr());
        // if method_id.is_null() {
        //     eprintln!("Failed to find getGreeting method.");
        //     detach_current_thread();
        //     return;
        // }
        // println!("Found getGreeting method.");

        let result = (**env).CallObjectMethod.unwrap()(env, obj, method_id);
        if result.is_null() {
            eprintln!("Failed to call getGreeting method.");
            detach_current_thread();
            return;
        }
        println!("Called getGreeting method.");

        let result_str_ptr = (**env).GetStringUTFChars.unwrap()(env, result, ptr::null_mut());
        if result_str_ptr.is_null() {
            eprintln!("Failed to convert Java string to Rust string.");
            detach_current_thread();
            return;
        }

        let result_str = CStr::from_ptr(result_str_ptr)
            .to_string_lossy()
            .into_owned();
        println!("Result from Java: {}", result_str);

        (**env).ReleaseStringUTFChars.unwrap()(env, result, result_str_ptr);

        // Detach the current thread from the JVM
        detach_current_thread();

        // Note: We're not destroying the JVM here because it's generally not recommended
        // to destroy the JVM unless you're absolutely sure you want to terminate the entire Java environment.
        // If you do need to destroy the JVM, you can uncomment the following code:

        /*
        // Destroy the JVM
        let destroy_res = (**vm).DestroyJavaVM.unwrap()(vm);
        if destroy_res != 0 {
            eprintln!("Failed to destroy JVM.");
        } else {
            println!("Destroyed JVM.");
        }
        */
    }
}

pub unsafe fn new_object(
    env: *mut jni_sys::JNIEnv,
    class_name: CString,
    method_name: CString,
    method_sig: CString,
) -> (*mut _jobject, *mut _jmethodID) {
    let find_class = (**env).FindClass.unwrap();
    let get_method_id = (**env).GetMethodID.unwrap();
    let new_object_here = (**env).NewObject.unwrap();

    let class = find_class(env, class_name.as_ptr() as _);
    if class.is_null() {
        eprintln!("Failed to find {:?} class.", class_name);
        detach_current_thread();
        // return;
    }
    println!("Found {:?} class.", class_name);

    let method_id = get_method_id(
        env,
        class,
        method_name.as_ptr() as _,
        method_sig.as_ptr() as _,
    );
    if method_id.is_null() {
        eprintln!("Failed to find constructor method.");
        detach_current_thread();
        // return;
    }
    println!("Found constructor method.");

    let obj = new_object_here(env, class, method_id);
    if obj.is_null() {
        eprintln!("Failed to create {:?} object.", class_name);
        detach_current_thread();
        // return;
    }
    println!("Created {:?} object.", class_name);
    (obj, method_id)
}

pub unsafe fn detach_current_thread() {
    // eprintln!("Failed to find constructor method.");
    let detach_res = (**get_java_vm()).DetachCurrentThread.unwrap()(get_java_vm());
    if detach_res != 0 {
        eprintln!(
            "Failed to detach current thread. Error code: {}. Ensure the thread is properly attached and the JVM state is correct.",
            detach_res
        );
    } else {
        println!("Detached current thread from JVM.");
    }
}

pub unsafe fn attach_jni_env() -> *mut jni_sys::JNIEnv {
    let mut env: *mut jni_sys::JNIEnv = std::ptr::null_mut();
    let attach_current_thread = (**get_java_vm()).AttachCurrentThread.unwrap();

    let res = attach_current_thread(get_java_vm(), &mut env, std::ptr::null_mut());
    // assert!(res == 0);
    if res != 0 || env.is_null() {
        eprintln!(
            "Failed to attach current thread or env is null. Error code: {}",
            res
        );
        return env;
    }
    println!("Attached current thread to JVM.");
    env
}

#[no_mangle]
pub extern "C" fn test_attach_detach() {
    unsafe {
        if get_java_vm().is_null() {
            eprintln!("Failed to get JavaVM");
            return;
        }

        let _ = attach_jni_env();

        detach_current_thread();
    }
}

#[no_mangle]
pub extern "C" fn Java_HelloWorld_callRustFunction(_env: *mut jni_sys::JNIEnv, _obj: jobject) {
    // Implementation of your native method
    // println!("Rust function callRustFunction is called from Java!");
    // Optionally, you can call other Rust functions like call_java_from_rust() here if needed
    call_java_from_rust();
    // test_attach_detach();
}

#[no_mangle]
pub extern "system" fn JNI_OnLoad(vm: *mut JavaVM, _reserved: *const std::ffi::c_void) -> jint {
    unsafe {
        VM = vm;
    }
    jni_sys::JNI_VERSION_1_6
}

#[no_mangle]
pub extern "C" fn JNI_OnUnload(_vm: *mut JavaVM, _reserved: *const std::ffi::c_void) {
    // unsafe {
    //     // Optionally, you can add cleanup code here
    // }
}
