//File lib.rs

// src/lib.rs
// src/java.rs
// src/rust_interop.rs
// src/c.rs
// examples/c.rs
// examples/java.rs
// examples/swift.rs
// src/main.rs
// src/objc.rs
// src/swift
// c/example.h
// c/example.c
// java/HelloWorld.java
// java/HelloWorld.class
// objc/MyClass.h
// objc/MyClass.m
// swift/MySwiftClass.swift
// swift/SwiftModule.h
// swift/module.modulemap
// Cargo.toml
// build.rs
pub mod c;
pub mod java;
pub mod objc;
pub mod rust_interop;
pub mod swift;

pub use rust_interop::add_and_print; // Re-export the function for easier access
