use std::ffi::c_char;

extern "C" {
    pub fn create_object();
    pub fn call_method();
    pub fn get_message() -> *const c_char;
}
