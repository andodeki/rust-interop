// src/rust_interop.rs
pub use crate::c::{add, print_message};

pub fn add_and_print(a: i32, b: i32) {
    let result = unsafe { add(a, b) };
    println!("The result of adding {} and {} is: {}", a, b, result);
    unsafe {
        print_message();
    }
}
