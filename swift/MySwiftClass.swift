// swift/MySwiftClass.swift
import Foundation

@_silgen_name("swift_function")
func swiftFunction() -> Int32 {
    print("Swift function called")
    return 42
}
