// swift/SwiftModule.h
#ifndef SWIFT_MODULE_H
#define SWIFT_MODULE_H

#ifdef __cplusplus
extern "C" {
#endif

int32_t swiftFunction();

#ifdef __cplusplus
}
#endif

#endif // SWIFT_MODULE_H
